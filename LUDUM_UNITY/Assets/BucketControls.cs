﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MovementState
{
  grounded,
  airborne
}

public class BucketControls : MonoBehaviour
{
  public Transform TopLeft;
  public Transform TopRight;
  public Transform BotLeft;
  public Transform BotRight;

  public float Thrust = 100;
  public Rigidbody2D myBody;

  public float RetractMaxSpeed = 10;
  public float RetractAcceleration = 10;
  public float ExpandSpeed = 10;

  public MovementState state;
  public ContactFilter2D GroundedFilter;
  readonly float contactTreshold = Mathf.Cos(30*Mathf.Deg2Rad);

  public float maxGroundedVelocityHorizontal = 5;
  public float AccelerationHorizontal = 20;

  public Rope rope;
  internal Vector2 ConnectionPoint
  {
    get {
      return ((Vector3)(transform.localToWorldMatrix * (Vector3)GetComponent<SpringJoint2D>().anchor) + transform.localPosition); }
  }

  [SerializeField][ReadOnly]bool isGrounded = false;

  ContactPoint2D[] groundContacts = new ContactPoint2D[16];
  [SerializeField] [ReadOnly] Vector2 AverageGroundNormal;
  Rigidbody2D body;

  float GroundVelocity;
  float GroundAcceleration;

  private void OnEnable()
  {
    body = GetComponent<Rigidbody2D>();
    isGrounded = true;
    state = MovementState.grounded;
  }

  private void FixedUpdate()
  {
    //determine if we are standing on platform
    int countGroundContacts = body.GetContacts(GroundedFilter, groundContacts);
    bool isGroundedThisFrame = false;
    AverageGroundNormal = Vector3.zero;
    for (int i = 0; i < countGroundContacts; i++)
    {
      float cos = Vector2.Dot(groundContacts[i].normal, Vector2.up);
      bool isGroundContact = isGroundedThisFrame || (cos > contactTreshold);
      if (isGroundContact)
      {
        AverageGroundNormal += groundContacts[i].normal;
      }
      isGroundedThisFrame |= isGroundContact;
    };

    if (isGroundedThisFrame)
    {
      AverageGroundNormal.Normalize();
    }
    else
    {
      AverageGroundNormal = Vector3.zero;
    }

    if (isGroundedThisFrame && !isGrounded)
    {
      OnLanding();
    }
    if (!isGroundedThisFrame && isGrounded)
    {
      OnLiftoff();
    }
    isGrounded = isGroundedThisFrame;
  }

  private void OnLanding()
  {
    Debug.Log("landed");
    float bodyAngle = Vector2.Angle(Vector2.up, AverageGroundNormal);
    body.SetRotation(bodyAngle);
    Debug.Log($"set angle to {bodyAngle}");
    Vector2 velocityDir = Vector2.Perpendicular(AverageGroundNormal);
    float velocityValue = Vector2.Dot(AverageGroundNormal, body.velocity);
    body.velocity = velocityDir * velocityValue;
    state = MovementState.grounded;
    body.constraints = body.constraints | RigidbodyConstraints2D.FreezeRotation;
  }

  private void OnLiftoff()
  {
    Debug.Log("Liftoff");
    state = MovementState.airborne;
    body.constraints = body.constraints & ~RigidbodyConstraints2D.FreezeRotation;
  }

  void Update()
  {
    if(state == MovementState.grounded)
    {
      ConsumeGroundedInput();
    }
    else
    {
      ConsumeAirborbeInpout();
    }

    //rope manipulation in all modes
    rope.Retract(Input.GetKey(KeyCode.W));

    if (Input.GetKey(KeyCode.S))
    {
      rope.Expand(ExpandSpeed);
    }
  }

  private void ConsumeAirborbeInpout()
  {
    var r = transform.right;
    if (Input.GetKey(KeyCode.Q))
    {
      ApplyThrust(-r * Thrust, TopLeft.transform.position);
    }
    if (Input.GetKey(KeyCode.A))
    {
      ApplyThrust(-r * Thrust, BotLeft.transform.position);
    }
    if (Input.GetKey(KeyCode.E))
    {
      ApplyThrust(r * Thrust, TopRight.transform.position);
    }
    if (Input.GetKey(KeyCode.D))
    {
      ApplyThrust(r * Thrust, BotRight.transform.position);
    }

  }

  private void ConsumeGroundedInput()
  {
    var r = transform.right;
    if (Input.GetKey(KeyCode.A))
    {
      if(body.velocity.x > -maxGroundedVelocityHorizontal)
      {
        body.AddForce(-r * AccelerationHorizontal, ForceMode2D.Force);
      }
    }

    if (Input.GetKey(KeyCode.D))
    {
      if (body.velocity.x < maxGroundedVelocityHorizontal)
      {
        body.AddForce(r * AccelerationHorizontal, ForceMode2D.Force);
      }
    }
  }

  private void ApplyThrust(Vector2 dir, Vector2 point)
  {
    myBody.AddForceAtPosition(dir * Time.deltaTime, point, ForceMode2D.Force);
    Debug.DrawLine(point, point + dir.normalized);
  }
}
