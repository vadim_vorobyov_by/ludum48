﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using UnityEngine;

public class Rope : MonoBehaviour
{
  public BucketControls Bucket;
  public GameObject Root;

  struct StickPoint
  {
    public Vector2 position;
    public Vector2 tensionNormal;
  }

  LinkedList<StickPoint> StickyPoints = new LinkedList<StickPoint>();

  public float Totallength = 6;

  public ContactFilter2D filter;

  float FreeSegmentLength => Totallength - constrainedLength;

  [SerializeField, ReadOnly]
  private float retractSpeed = 0;
  [SerializeField, ReadOnly]
  bool isRetracting = false;

  public float maxRetractSpeed = 10;
  public float retractAcceleration = 10;

  public float constrainedLength = 0;

  internal void Retract(bool isOn)
  {
    isRetracting = isOn;
  }

  internal void Expand(float speed)
  {
    Totallength += speed * Time.deltaTime;
  }

  private void Start()
  {
    StickyPoints.AddFirst(new StickPoint
    {
      position = Root.transform.position,
      tensionNormal = Vector2.zero
    });
  }

  private void Update()
  {

    if(isRetracting)
    {
      retractSpeed = Mathf.Clamp(retractSpeed + retractAcceleration * Time.deltaTime, 0, maxRetractSpeed);
      Totallength -= retractSpeed * Time.deltaTime;
    }
    else
    {
      retractSpeed = 0;
    }
    Bucket.GetComponent<SpringJoint2D>().distance = FreeSegmentLength;


    DebugDraw2D.Cross(Bucket.ConnectionPoint);

    int mask = LayerMask.GetMask("Default");

    //check for node disappearing

    var freeSegmentDir = Bucket.ConnectionPoint - StickyPoints.Last.Value.position;
    if (Vector2.Dot(freeSegmentDir, StickyPoints.Last.Value.tensionNormal) > 0)
    {
      Debug.Log("remove");
      StickyPoints.RemoveLast();
      EaluateConstrainedLength();

      //method: reattach node
      Bucket.GetComponent<SpringJoint2D>().connectedBody.transform.position = StickyPoints.Last.Value.position;
      Bucket.GetComponent<SpringJoint2D>().distance = FreeSegmentLength;
    }

    //check for new node appearing
    Debug.DrawLine(Bucket.ConnectionPoint, StickyPoints.Last.Value.position);
    var RopeDirection = Bucket.ConnectionPoint - StickyPoints.Last.Value.position;

    RaycastHit2D[] hitResults = new RaycastHit2D[4];
    RaycastHit2D[] reversehitResults = new RaycastHit2D[4];
    //filter.layerMask = mask;
    int hitCount = Physics2D.LinecastNonAlloc(StickyPoints.Last.Value.position, Bucket.ConnectionPoint, hitResults, mask);
    for(int i =0; i < hitCount; i++)
    {
      if(i > 1)
      {
        Debug.Log("MOAR");
      }

      var hit = hitResults[i];

      if(hit.fraction == 0.0f)
      {
        //when fraction is 0 we start linecast from edge or inside of a rigidbody. thus it's a false-positive;
        continue;
      }

      DebugDraw2D.Cross(hit.point, Color.cyan);
      Debug.DrawLine(hit.point, hit.point + hit.normal, Color.yellow);

      Vector2 Corner = hit.point;
      Vector2 tensionnormal = hit.normal; 

      //hit refinement
      var reverseHitCount = Physics2D.LinecastNonAlloc(Bucket.ConnectionPoint, StickyPoints.Last.Value.position, reversehitResults, mask);
      if(reverseHitCount > 0)
      {
        DebugDraw2D.Cross(reversehitResults[0].point, Color.magenta);
        Debug.DrawLine(reversehitResults[0].point, reversehitResults[0].point + reversehitResults[0].normal, Color.yellow);
        Corner = Geometry2D.CalcLineIntersectionPoint(reversehitResults[0].point, reversehitResults[0].normal, hit.point, hit.normal);
        Corner += (Vector2)((hit.normal + reversehitResults[0].normal).normalized * 0.05f);
      }

      DebugDraw2D.Cross(Corner, Color.white);

      if (Vector2.SqrMagnitude(StickyPoints.Last.Value.position - Corner) > 0.04f /*0.1 squared*/)
      {
        Debug.Log("add");
        StickyPoints.AddLast(new StickPoint { position = Corner, tensionNormal = hit.normal });
        EaluateConstrainedLength();

        //method: attach to node
        Bucket.GetComponent<SpringJoint2D>().connectedBody.transform.position = StickyPoints.Last.Value.position; 
        Bucket.GetComponent<SpringJoint2D>().distance = FreeSegmentLength;
      }
    }

    //if rope is sloppy, it should behave sloppy
    float distToNode = Vector2.Distance(StickyPoints.Last.Value.position, Bucket.ConnectionPoint);
    if (distToNode < Bucket.GetComponent<SpringJoint2D>().distance)
    {
      Bucket.GetComponent<SpringJoint2D>().enabled = false;
    }
    else
    {
      Bucket.GetComponent<SpringJoint2D>().enabled = true;
    }

    //debugs
    foreach (var p in StickyPoints)
    {
      DebugDraw2D.Cross(p.position, 0.1f, Color.blue);
      Debug.DrawLine(p.position, p.position + p.tensionNormal, Color.red);
    }
  }

  private Vector2 FindCornerPoint(Vector2 P1, Vector2 normal1, Vector2 point2, Vector2 normal2)
  {
    throw new NotImplementedException();
  }

  private void EaluateConstrainedLength()
  {
    constrainedLength = 0;
    var node = StickyPoints.First;
    if(node == null)
    {
      return;
    }
    while (node.Next != null)
    {
      constrainedLength += (node.Next.Value.position - node.Value.position).magnitude;
      node = node.Next;
    }
  }
}
