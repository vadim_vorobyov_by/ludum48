﻿using System;
using UnityEngine;
using UnityEditor;

public static class Geometry2D
{
  public static Vector2 CalcLineIntersectionPoint(Vector2 p1, Vector2 n1, Vector2 p2, Vector2 n2)
  {
    //if (Mathf.Abs(n1.sqrMagnitude - 1) > Mathf.Epsilon) throw new ArgumentException($"n1 is not normalized, magnitude is {n1.magnitude}");
    //if (Mathf.Abs(n2.sqrMagnitude - 1) > Mathf.Epsilon) throw new ArgumentException($"n2 is not normalized, magnitude is {n2.magnitude}");
    if (n1 == n2)
    {
      if (p1 == p2) throw new InvalidOperationException("lines are the same");
      else throw new InvalidOperationException("lines don't intersect");
    }

    n1.Normalize();
    n2.Normalize();

    //Krummer method for solving system of linear equations

    float c1 = n1.x * p1.x + n1.y * p1.y;
    float c2 = n2.x * p2.x + n2.y * p2.y;

    float detMain = n1.x * n2.y - n1.y * n2.x;  //+
    float det_x = c1 * n2.y - c2 * n1.y;
    float det_y = c2 * n1.x - c1 * n2.x;        //+

    return new Vector2(det_x / detMain, det_y / detMain);
  }
}