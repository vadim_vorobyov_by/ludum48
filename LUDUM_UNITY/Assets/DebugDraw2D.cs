﻿using UnityEngine;
using UnityEditor;

public static class DebugDraw2D
{
  const float defaultSize = 0.5f;

  public static void Cross(Vector2 point)
  {
    Cross(point, defaultSize, Color.white, 0, true);
  }

  public static void Cross(Vector2 point, float size, float duration = 0.0f, bool depthTest = true)
  {
    Cross(point, size, Color.white, duration, depthTest);
  }

  public static void Cross(Vector2 point, Color color, float duration = 0.0f, bool depthTest = true)
  {
    Cross(point, defaultSize, color, duration, depthTest);
  }

  public static void Cross(Vector2 point, float size, Color color, float duration = 0.0f, bool depthTest = true)
  {
    Vector2 L = new Vector2(size, 0);
    Vector2 U = new Vector2(0, size);
    Debug.DrawLine(point - L, point + L, color, duration, depthTest);
    Debug.DrawLine(point - U, point + U, color, duration, depthTest);
  }
}