﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTracker : MonoBehaviour
{
  public GameObject Tracked;

  public float Speed = 4;

  // Start is called before the first frame update
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {
    var target = Tracked.transform.position;
    var location = transform.position;
    var move = target - location;
    move.z = 0;

    var step = Vector3.ClampMagnitude(move, Speed * Time.deltaTime);

    transform.localPosition += step;
  }
}
